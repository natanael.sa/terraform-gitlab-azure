variable "location" {
  description = "Regiao da Azure"
  type        = string
  default     = "West US 3"
}

variable "azure_pub_key" {
  description = "Public Key para VM na Azure"
  type        = string 
}